// SPDX-FileCopyrightText: 2023 Michael Pöhn <michael@poehn.at>
// SPDX-License-Identifier: GPL-3.0-or-later

import 'package:flutter/material.dart';
import 'package:fdroidwebdash/app.dart';

void main() {
  runApp(const WebDashApp());
}
